-- Best practice MySQL as of 5.7.6
--
-- This script needs to run only once

DROP DATABASE IF EXISTS JSF_COMPANY;
CREATE DATABASE JSF_COMPANY;

USE JSF_COMPANY;

DROP USER IF EXISTS clientdev@localhost;
CREATE USER clientdev@'localhost' IDENTIFIED WITH mysql_native_password BY 'caterpillar' REQUIRE NONE;
GRANT ALL ON JSF_COMPANY.* TO clientdev@'localhost';

FLUSH PRIVILEGES;
