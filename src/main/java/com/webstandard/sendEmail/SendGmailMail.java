package com.webstandard.sendEmail;

import java.io.Serializable;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import jodd.mail.SmtpServer;
import jodd.mail.SendMailSession;
import jodd.mail.Email;
import jodd.mail.EmailMessage;
import jodd.util.MimeTypes;

/**
 * Send GMail message using SMTP SSL.
 */

@Named("sendGmailMail")
//@RequestScoped
public class SendGmailMail implements Serializable {
    private String emailAdress;
    public void sendMail() {
        SendMailSession session = null;
        FacesContext context = FacesContext.getCurrentInstance();
        emailAdress= getAdress(context);
        try {
              
            Email email = new Email();
            email.from("hexiao", "zhanghexiao88@gmail.com");
            email.to("Clients", emailAdress);
            email.setSubject("test");
            EmailMessage textMessage = new EmailMessage("Welcome to Montreal News", MimeTypes.MIME_TEXT_PLAIN);
            email.addMessage(textMessage);
     
            SmtpServer<?> smtpServer = SmtpServer.create("zhanghexiao88@gmail.com")
                    .authenticateWith("your email address", "your email password").timeout(1000);
        
            session = smtpServer.createSession();
            session.open();
            String result = session.sendMail(email);
            session.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (null != session) {
                session.close();
            }
        }
    }
        private String getAdress(FacesContext context) {
        Map<String, String> params = context.getExternalContext()
                .getRequestParameterMap();
        return params.get("emailIntable");
    }
    
}
//
//
