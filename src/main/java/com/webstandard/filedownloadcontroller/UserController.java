package com.webstandard.filedownloadcontroller;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *The download method was created for download the file.
 * 
 */
@SessionScoped
@ManagedBean
public class UserController {

    public void download(int bookChoose) {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            ExternalContext externalContext = context.getExternalContext();            
            externalContext.responseReset();
            externalContext.setResponseContentType("text/webviewhtml");
            externalContext.setResponseHeader("Content-Disposition", "attachment;filename=\"book.html\"");
            FileInputStream inputStream;
            OutputStream outputStream;
            switch(bookChoose){                
            case 1:    
            inputStream = new FileInputStream(new File(".\\Book1.html"));
            outputStream = externalContext.getResponseOutputStream();
            break;
            case 2:    
            inputStream = new FileInputStream(new File(".\\Book2.html"));
            outputStream = externalContext.getResponseOutputStream();
            break;
            default:   
            inputStream = new FileInputStream(new File(".\\Book3.html"));
            outputStream = externalContext.getResponseOutputStream();    
            }
            byte[] buffer = new byte[1024];
            int length;
            while ((length = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
            }

            inputStream.close();
            context.responseComplete();
            downloadIf();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }       
    }
//I want to use this mehtod, which can be called in the download method to returen 
//a string "FrontPage", then i can automatically go back to front page, but i failed
    public String downloadIf(){
    return "FrontPage";
    }
}