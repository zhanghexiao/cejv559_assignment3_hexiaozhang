package com.webstandard.listener;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Phase Listener
 *
 * @author Ken
 */
public class LifeCycleListener implements PhaseListener {

    private final static Logger LOG = LoggerFactory.getLogger(LifeCycleListener.class);

    /**
     * Which phase to listen for
     *
     * @return
     */
    @Override
    public PhaseId getPhaseId() {
        return PhaseId.ANY_PHASE;
    }

    @Override
    public void beforePhase(PhaseEvent event) {
        LOG.info("START PHASE {0}", event.getPhaseId());
    }

    @Override
    public void afterPhase(PhaseEvent event) {
        LOG.info("END PHASE {0}", event.getPhaseId());
    }

}
