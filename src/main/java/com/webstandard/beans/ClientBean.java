package com.webstandard.beans;

import com.webstandard.jpa.controller.ClientJpaController;
import com.webstandard.entities.Client;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 */
@Named("theClients")
@SessionScoped
public class ClientBean implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(ClientBean.class);
        
    @Inject
    private ClientJpaController clientJpaController;

    public List<Client> getClients() {

        return clientJpaController.findClientEntities();
    }
    

    
}

