package com.webstandard.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * This entity class has had the attribute 'message' added to the bean
 * validation annotations so that custom and localized messages can be used. The
 * messages can be found in ValidationMessages.properties that must be placed in
 * the root of the classpath. This means into the src/main/resources folder
 * without any package
 */
@Entity
@Table(name = "client", catalog = "JSF_COMPANY", schema = "")
@NamedQueries({
    @NamedQuery(name = "Client.findAll", query = "SELECT c FROM Client c")
    ,
    @NamedQuery(name = "Client.findByEmailAndPassword", query = "select c from Client c where c.email=?1 and c.password=?2")
    ,
    @NamedQuery(name = "Client.findByEmail", query = "SELECT c FROM Client c WHERE c.email = :email"),})
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CLIENT_NUMBER")
    private Integer clientNumber;

    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 1, max = 30, message = "{length}")
    @Column(name = "LAST_NAME")
    private String lastName;

    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 1, max = 30, message = "{length}")
    @Column(name = "FIRST_NAME")
    private String firstName;


    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 1, max = 45, message = "{length}")
    @Column(name = "ADDRESS_1")
    private String address1;

    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 1, max = 30, message = "{length}")
    @Column(name = "CITY")
    private String city;

    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 1, max = 25, message = "{length}")
    @Column(name = "PROVINCE")
    private String province;

    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 1, max = 30, message = "{length}")
    @Column(name = "COUNTRY")
    private String country;

    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 1, max = 7, message = "{length}")
    @Column(name = "POSTAL_CODE")
    private String postalCode;

    @Pattern(regexp = "[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?", message = "{invalidemail}")
    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 1, max = 60, message = "{length}")
    @Column(name = "EMAIL")
    private String email;

    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 2, max = 12, message = "{length}")
    @Column(name = "PASSWORD")
    private String password;

    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 2, max = 12, message = "{length}")
    @Column(name = "BOOKCHOICE")
    private String bookChoice;
    
    @Basic(optional = true)
    //@NotNull
    @Column(name = "DATE_ENTERED", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEntered;

    public Client() {
    }

    public Client(Integer clientNumber) {
        this.clientNumber = clientNumber;
    }

    public Client(Integer clientNumber, String lastName, String firstName, String address1, String city, String province, String country, String postalCode, String email, String password, Date dateEntered, String bookChoice) 
       
    {
        this.clientNumber = clientNumber;
        this.lastName = lastName;
        this.firstName = firstName;
        this.address1 = address1;
        this.city = city;
        this.province = province;
        this.country = country;
        this.postalCode = postalCode;
        this.email = email;
        this.password = password;
        this.dateEntered = dateEntered;
        this.bookChoice = bookChoice;
    }

    public Integer getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
        this.clientNumber = clientNumber;
    }


    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getDateEntered() {
        return dateEntered;
    }

    public void setDateEntered(Date dateEntered) {
        this.dateEntered = dateEntered;
    }
    
    public String getBookChoice() {
        return bookChoice;
    }

    public void setBookChoice(String bookChoice) {
        this.bookChoice = bookChoice;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clientNumber != null ? clientNumber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Client)) {
            return false;
        }
        Client other = (Client) object;
        if ((this.email == null && other.email != null) || (this.email != null && !this.email.equals(other.email))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.webstandard.entities.Client[ email=" + email + " ]";
    }

}
